﻿using System;
using TestLoad.Model.Data;
using TestLoad.Model.DomainObjects;
using Microsoft.Practices.Unity;
using TestLoad.DAL.Exceptions;

namespace TestLoad.DAL
{
    public sealed class UnitOfWork: IUnitOfWork
    {
        [Dependency]
        public DataContext Context { get; set; }

        [Dependency]
        public IRepository<Image> Images { get; set; }

        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                throw new DomainException(Resources.Resource.ErrorSaveChanges);
            }
        }

        public void Discard()
        {
            Context.Dispose();
            Context = new DataContext();
        }
    }
}