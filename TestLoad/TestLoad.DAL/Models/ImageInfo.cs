﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestLoad.DAL.Models
{
    public class ImageInfo
    {
        public int Id { get; set; }
        [Display(Name = "FileName", ResourceType = typeof(Resources.Resource))]
        public string FileName { get; set; }
        [Display(Name = "FilePath", ResourceType = typeof(Resources.Resource))]
        public string FilePath { get; set; }
        [Display(Name = "Size", ResourceType = typeof(Resources.Resource))]
        public long Size { get; set; }
        [Display(Name = "Added", ResourceType = typeof(Resources.Resource))]
        public DateTime Added { get; set; }
    }
}