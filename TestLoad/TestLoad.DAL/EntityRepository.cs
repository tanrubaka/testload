﻿using System.Linq;
using Microsoft.Practices.Unity;
using TestLoad.Model.Data;
using TestLoad.Model.DomainObjects;

namespace TestLoad.DAL
{
    internal sealed class EntityRepository<T> : IRepository<T> where T : class, IDomainObject, new()
    {
        [Dependency]
        public DataContext Context { get; set; }

        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        public void Delete(T obj)
        {
            Context.Set<T>().Remove(obj);
        }

        public void Add(T obj)
        {
            Context.Set<T>().Add(obj);
        }

        public T Get(int id)
        {
            return Context.Set<T>().SingleOrDefault(o => o.Id == id);
        }
    }
}