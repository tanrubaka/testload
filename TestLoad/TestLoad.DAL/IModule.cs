﻿namespace TestLoad.DAL
{
    public interface IModule
    {
        void Initialize();
    }
}