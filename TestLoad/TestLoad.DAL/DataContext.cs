﻿using System.Data.Entity;
using TestLoad.Model.DomainObjects;
using TestLoad.Configuration;

namespace TestLoad.DAL
{
    public class DataContext: DbContext
    {
        public DbSet<Image> Images { get; set; }

        public DataContext()
            : base(AppConfig.ConnectionString)
        { }
    }
}