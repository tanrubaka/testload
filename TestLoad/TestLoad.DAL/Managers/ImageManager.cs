﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Practices.Unity;
using TestLoad.DAL.Exceptions;
using TestLoad.DAL.IManagers;
using TestLoad.DAL.Models;
using TestLoad.Model.Data;
using TestLoad.Model.DomainObjects;

namespace TestLoad.DAL.Managers
{
    public class ImageManager: IImageManager
    {
        private readonly int _pageSize = Configuration.AppConfig.PageSize;

        [Dependency]
        public IUnitOfWork Repository { get; set; }


        public ImageInfo Get(int id)
        {
            return GetInner(Repository.Images.Get(id));
        }

        public int GetMaxPage()
        {
            return (int)Math.Ceiling(((double)Repository.Images.GetAll().Count())/ _pageSize);
        }

        public IList<ImageInfo> GetPage(bool? isAsc, int? page)
        {
            var query = Repository.Images.GetAll().AsQueryable();
            if (isAsc!=null)
            {
                query = (bool)isAsc? query.OrderBy(i => i.Added): query.OrderByDescending(i => i.Added);
            }
            else
            {
                query=query.OrderBy(i => i.Id);
            }
            if (page!=null)
            {
                query = query
                    .Skip((int)page* _pageSize)
                    .Take(_pageSize);
            }
            return query.Select(GetInner).ToList();
        }

        public IList<ImageInfo> GetAll()
        {
            return Repository.Images.GetAll().Select(GetInner).ToList();
        }

        public int Add(ImageInfo obj)
        {
            var newObject = new Image
            {
                FileName = obj.FileName,
                FilePath = obj.FilePath,
                Added = obj.Added,
                Size = obj.Size
            };
            Repository.Images.Add(newObject);
            Repository.SaveChanges();
            return newObject.Id;
        }

        public void Edit(ImageInfo obj)
        {
            var editObject = Repository.Images.Get(obj.Id);
            if (editObject != null)
            {
                editObject.FileName = obj.FileName;
                editObject.FilePath = obj.FilePath;
                editObject.Size = obj.Size;
                Repository.SaveChanges();
            }
            else
            {
                throw new DomainException(Resources.Resource.ObjectNotFound);
            }
        }

        public void Delete(int id)
        {
            var deletedObject = Repository.Images.Get(id);
            if (deletedObject == null)
            {
                throw new DomainException(Resources.Resource.ObjectNotFound);
            }
            Repository.Images.Delete(deletedObject);

            try
            {
                Repository.SaveChanges();
            }
            catch (SqlException ex)
            {
                ExceptionHelper.RaiseIfForeignKeyViolation(ex, Resources.Resource.ErrorDelete,
                    string.Format("ID = {0}.", id));

                throw;
            }
        }

        private ImageInfo GetInner(Image obj)
        {
            return new ImageInfo
            {
                Id = obj.Id,
                FileName = obj.FileName,
                FilePath = obj.FilePath,
                Added = obj.Added,
                Size = obj.Size
            };
        }
    }
}