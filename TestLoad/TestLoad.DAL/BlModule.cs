﻿using Microsoft.Practices.Unity;
using TestLoad.Model.DomainObjects;
using TestLoad.Model.Data;

namespace TestLoad.DAL
{
    public sealed class BlModule : IModule
    {
        private readonly IUnityContainer _container;

        public BlModule(IUnityContainer container)
        {
            _container = container;
        }

        public void Initialize()
        {
            _container
                .RegisterType<IRepository<Image>, EntityRepository<Image>>(new HierarchicalLifetimeManager())
                .RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager());
        }
    }
}