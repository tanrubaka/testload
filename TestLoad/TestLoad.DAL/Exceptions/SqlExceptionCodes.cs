﻿namespace TestLoad.DAL.Exceptions
{
    internal sealed class SqlExceptionCodes
    {
        public const int ForeignKeyViolation = 547;
    }
}