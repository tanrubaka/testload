﻿using System;
using System.Runtime.Serialization;

namespace TestLoad.DAL.Exceptions
{
    [Serializable]
    public sealed class DomainException : Exception
    {
        public DomainException(string message) : base(message) { }

        public DomainException(string message, Exception inner) : base(message, inner) { }

        public DomainException(string message, string details)
            : this(message, details, null)
        {
        }

        public DomainException(string message, string details, Exception inner)
            : base(message, inner)
        {
            Details = details;
        }

        protected DomainException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }

        public string Details { get; private set; }
    }
}