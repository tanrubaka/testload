﻿using System.Data.SqlClient;

namespace TestLoad.DAL.Exceptions
{
    internal class ExceptionHelper
    {
        public static void RaiseIfForeignKeyViolation(SqlException ex, string message, string details)
        {
            if (ex.Number == SqlExceptionCodes.ForeignKeyViolation)
            {
                throw new DomainException(message, details, ex);
            }
        }
    }
}