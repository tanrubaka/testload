﻿using System.Collections.Generic;
using TestLoad.DAL.Models;

namespace TestLoad.DAL.IManagers
{
    public interface IImageManager
    {
        ImageInfo Get(int id);
        int GetMaxPage();
        IList<ImageInfo> GetAll();
        IList<ImageInfo> GetPage(bool? isAsc, int? page);
        int Add(ImageInfo obj);
        void Edit(ImageInfo obj);
        void Delete(int id);
    }
}