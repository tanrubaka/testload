﻿using System;
using System.Configuration;

namespace TestLoad.Configuration
{
    public static class AppConfig
    {
        public static string ConnectionString =>
            ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public static string FilePholder =>
            ConfigurationManager.AppSettings["FilePholder"];

        public static long MaxFileSize
        {
            get
            {
                long value;
                if (long.TryParse(ConfigurationManager.AppSettings["MaxFileSize"],out value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorMaxFileSize);
            }
        }


        public static int PageSize
        {
            get
            {
                int value;
                if (int.TryParse(ConfigurationManager.AppSettings["PageSize"], out value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorPageSize);
            }
        }

        public static int TimeZone
        {
            get
            {
                int value;
                if (int.TryParse(ConfigurationManager.AppSettings["TimeZone"], out value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorTimeZone);
            }
        }

        public static TimeSpan LoadRangeTime
        {
            get
            {
                TimeSpan value;
                if (TimeSpan.TryParse(ConfigurationManager.AppSettings["LoadRangeTime"], out value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorLoadRangeTime);
            }
        }
    }
}
