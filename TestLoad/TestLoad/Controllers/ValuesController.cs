﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TestLoad.DAL.IManagers;
using TestLoad.Models;
using Microsoft.Practices.Unity;
using TestLoad.Configuration;
using TestLoad.DAL.Models;
using TestLoad.Utils.Pictures;

namespace TestLoad.Controllers
{
    public class ValuesController : ApiController
    {
        private const char Quote = '\"';

        [Dependency]
        public IImageManager ImageManager { get; set; }
        
        public IHttpActionResult GetMaxPage()
        {
            try
            {
                return Ok(ImageManager.GetMaxPage());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Upload()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    return BadRequest();
                }
                var provider = new MultipartMemoryStreamProvider();
                await Request.Content.ReadAsMultipartAsync(provider);

                var url = new List<LoadFile>();
                foreach (var file in provider.Contents)
                {
                    var filename = file.Headers.ContentDisposition.FileName.Trim(Quote);

                    if (file.Headers.ContentLength>AppConfig.MaxFileSize)
                    {
                        return BadRequest(
                            $"{filename} {Resources.Resource.ErrorMaxSize} {Utils.SizeConvert.GetString(AppConfig.MaxFileSize)}");
                    }

                    byte[] fileArray = await file.ReadAsByteArrayAsync();

                    var savedFileName = await FileLoader.Upload(filename, fileArray);

                    url.Add(new LoadFile
                    {
                        FileName = file.Headers.ContentDisposition.Name.Trim(Quote),
                        Path = Utils.Pictures.FileLoader.GetPictureUrl(savedFileName)
                    });

                    ImageManager.Add(new ImageInfo
                    {
                        FileName = savedFileName,
                        FilePath = FileLoader.GetFilePath(savedFileName),
                        Added = DateTime.UtcNow,
                        Size = file.Headers.ContentLength??0
                    });
                }
                return Ok(url);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
