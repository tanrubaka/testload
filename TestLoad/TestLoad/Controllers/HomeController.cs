﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using TestLoad.DAL.IManagers;
using TestLoad.Models;

namespace TestLoad.Controllers
{
    public class HomeController : Controller
    {
        [Dependency]
        public IImageManager ImageManager { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Uploader()
        {
            return View();
        }


        public ActionResult List(bool? isAsc, int? page)
        {
            var imgs = ImageManager.GetPage(isAsc, page);
            var index = page != null ? Configuration.AppConfig.PageSize * (int)page : 0;
            var model = imgs.Select(img =>
            {
                var obj = new ImageView(img) {Number = ++index};
                return obj;
            }).ToList();
            
            return PartialView(model);
        }

    }
}