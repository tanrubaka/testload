﻿using System;
using System.ComponentModel.DataAnnotations;
using TestLoad.DAL.Models;

namespace TestLoad.Models
{
    public class ImageView : ImageInfo
    {
        [Display(Name = "StrSize", ResourceType = typeof(Resources.Resource))]
        public string StrSize { get; set; }
        [Display(Name = "Url", ResourceType = typeof(Resources.Resource))]
        public string Url { get; set; }
        public bool Backlight { get; set; }
        [Display(Name = "Number", ResourceType = typeof(Resources.Resource))]
        public int Number { get; set; }

        public ImageView()
        {
            
        }

        public ImageView(ImageInfo obj)
        {
            Id = obj.Id;
            FileName = obj.FileName;
            FilePath = obj.FilePath;
            Added = Utils.TimeConvert.Convert(obj.Added, Configuration.AppConfig.TimeZone);
            Size = obj.Size;
            StrSize = Utils.SizeConvert.GetString(obj.Size);
            Url = Utils.Pictures.FileLoader.GetPictureUrl(obj.FileName);
            Backlight = obj.Added.Add(Configuration.AppConfig.LoadRangeTime) >= DateTime.UtcNow;
        }
    }
}