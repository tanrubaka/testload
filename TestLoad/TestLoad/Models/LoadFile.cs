﻿namespace TestLoad.Models
{
    public sealed class LoadFile
    {
        public int Index { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
    }
}