﻿var curPage = 0;
var curIsAsc = null;
var maxPage = 0;

function NextPage() {
    var page = curPage + 1;
    GetPage(null, page >= maxPage ? maxPage : page);
}

function PrevPage() {
    var page = curPage - 1;
    GetPage(null, page < 0 ? 0 : page);
}

function GetPage(isAsc, page) {
    $('#busy').show();
    if (isAsc != null) {
        curIsAsc = isAsc;
    }

    if (page != null) {
        curPage = parseInt(page);
    }
    var url = '/Home/List?isAsc=' + curIsAsc + '&page=' + curPage;
    var files = $("#files");
    $.get(url).success(
    function (response, status, jqXhr) {
        files.empty().append(response);
    }).error(function (response, status, jqXhr) {
        $('#errors').append(JSON.parse(response.responseText).Message + "<br/>");
        $('#busy').hide();
    }).complete(function (response, status, jqXhr) {
        PageLoaded();
        $('#busy').hide();
    });
}

function PageLoaded() {
    if (curIsAsc === 'true') {
        $('#ordre').hide();
        $('#ordre-desc').show();
    } else {
        $('#ordre').show();
        $('#ordre-desc').hide();
    }
    $.ajax({
        type: "GET",
        url: '/api/values/GetMaxPage',
        contentType: false,
        processData: false
    })
        .success(function (result) {
            maxPage = result;
            var pager = document.getElementById('pagination');
            $('#pagination li').remove();
            if (curPage >0) {
                pager.appendChild(Navigation("Previous", "PrevPage()", '<'));
            }

            for (var i = 0; i < maxPage; i++) {
                var page = document.createElement('li');
                var pageA = document.createElement('a');
                pageA.setAttribute("onclick", "GetPage(null, " + i + ")");
                pageA.setAttribute("href", "#");
                if (curPage === i) {
                    page.className = "active";
                }
                var t = document.createTextNode(i+1);
                pageA.appendChild(t);
                page.appendChild(pageA);
                pager.appendChild(page);
            }

            if (curPage < maxPage-1) {
                pager.appendChild(Navigation("Next",  "NextPage()",'>'));
            }
            insertParam("isAsc", curIsAsc);
            insertParam("page", curPage);
        }).
        error(function (xhr, status) {
            $('#errors').append(JSON.parse(xhr.responseText).Message + "<br/>");
        });
}

function Navigation(label, clickFunction, text) {
    var next = document.createElement('li');
    var nextA = document.createElement('a');
    nextA.setAttribute("aria-label", label);
    nextA.setAttribute("href", "#");
    nextA.setAttribute("onclick", clickFunction);
    var nextSpan = document.createElement('span');
    nextSpan.appendChild(document.createTextNode(text));
    nextSpan.setAttribute("aria-hidden", "true");
    nextA.appendChild(nextSpan);
    next.appendChild(nextA);
    return next;
}

function ShowImg(img) {
    var div = document.getElementById('show-img');
    div.style.left = event.clientX + "px";
    div.style.top = event.clientY + "px";
    $('#show-img').show();
    $('#showing').attr("src", img);
}


function HideImg() {
    $('#show-img').hide();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function insertParam(key, value) {
    key = encodeURIComponent(key); value = encodeURIComponent(value);

    var s = document.location.search;
    var kvp = key + "=" + value;

    var r = new RegExp("(&|\\?)" + key + "=[^\&]*");

    s = s.replace(r, "$1" + kvp);

    if (!RegExp.$1) { s += (s.length > 0 ? '&' : '?') + kvp; };

    window.history.pushState('', '', s);
}