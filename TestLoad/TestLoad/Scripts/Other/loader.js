﻿function UpdateFiles(e) {
    var files = e.files;
    $('#files').empty();
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            for (var x = 0; x < files.length; x++) {
                var div = document.createElement('div');
                var row = document.createElement('div');
                row.className = "row";

                var h = document.createElement('H3');
                var t = document.createTextNode(files[x].name);
                h.appendChild(t);

                var colSourceImg = document.createElement('div');
                colSourceImg.className = "col-xs-12 col-md-4";
                var img = document.createElement('img');
                img.className = "preview";
                img.src = window.URL.createObjectURL(files[x]);
                img.setAttribute("id", "source-img-" + x);
                img.setAttribute("onMouseOver", "RealImgSet('"+img.src+"')");;
                img.setAttribute("onMouseOut", "RealImgClear()");
                colSourceImg.appendChild(img);

                var colProgress = document.createElement('div');
                colProgress.className = "col-xs-12 col-md-4";
                var progressHtml = document.createElement('div');
                progressHtml.className = "progress";
                var progress = document.createElement('div');
                progress.className = "progress-bar";
                progress.setAttribute("id", "progress-bar-" + x); 
                progressHtml.appendChild(progress);
                colProgress.appendChild(progressHtml);

                var colImg = document.createElement('div');
                colImg.className = "col-xs-12 col-md-4";
                colImg.setAttribute("id", "file-" + x);

                row.appendChild(colSourceImg);
                row.appendChild(colProgress);
                row.appendChild(colImg);
                div.appendChild(h);
                div.appendChild(row);
                $('#files').append(div);
            }
        }
    }
}

function LoadFiles () {
    var files = document.getElementById('upload-file').files;
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            for (var x = 0; x < files.length; x++) {
                var data = new FormData();
                data.append("file-" + x, files[x]);
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.progress = $("#progress-bar-" + x);
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = parseInt(evt.loaded / evt.total * 100);

                                this.progress.css('width', percentComplete + '%')
                                    .attr('aria-valuenow', percentComplete)
                                    .text(percentComplete + " %");
                                
                            }
                        }, false);

                        return xhr;
                    },
                    type: "POST",
                    url: '/api/values/upload',
                    contentType: false,
                    processData: false,
                    data: data
                })
                .success(function (result) {
                    $.each(result, function (index, element) {
                        var img = document.createElement('img');
                        img.className = "loaded";
                        img.src = element.Path;
                        img.setAttribute("onMouseOver", "RealImgSet('" + img.src + "')");;
                        img.setAttribute("onMouseOut", "RealImgClear()");
                        $('#' + element.FileName).append(img);
                        });
                    }).
                error(function (xhr, status) {
                    $('#errors').append(JSON.parse(xhr.responseText).Message+"<br/>");
                });
            }
        }
        else {
            alert("Браузер не поддерживает загрузку файлов HTML5!");
        }
    }
}

function RealImgSet(source) {
    $('#real-img').attr('src' , source);
}

function RealImgClear() {
    $('#real-img').attr('src', '');
}

