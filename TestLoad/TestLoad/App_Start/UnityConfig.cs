using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using TestLoad.Controllers;
using Unity.Mvc5;

namespace TestLoad
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IController, HomeController>("Home");

            container.RegisterType<DAL.IManagers.IImageManager, DAL.Managers.ImageManager>(new HierarchicalLifetimeManager());
            container.RegisterType<DAL.DataContext>(new HierarchicalLifetimeManager());

            DAL.IModule dalModul = new DAL.BlModule(container);
            dalModul.Initialize();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}