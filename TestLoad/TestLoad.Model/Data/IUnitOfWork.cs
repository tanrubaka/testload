﻿using TestLoad.Model.DomainObjects;

namespace TestLoad.Model.Data
{
    public interface IUnitOfWork
    {
        IRepository<Image> Images { get; }
        void SaveChanges();
        void Discard();
    }
}