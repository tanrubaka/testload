﻿using System.Linq;
using TestLoad.Model.DomainObjects;

namespace TestLoad.Model.Data
{
    public interface IRepository<T>
        where T : IDomainObject, new()
    {
        IQueryable<T> GetAll();
        void Delete(T obj);
        void Add(T obj);
        T Get(int id);
    }
}