﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestLoad.Model.DomainObjects
{
    public class Image : IDomainObject
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public long Size { get; set; }
        public DateTime Added { get; set; }
    }
}