﻿namespace TestLoad.Utils.Enums
{
    public enum ByteSize
    {
        Byte = 0,
        Kbyte = 1,
        Mbyte = 2,
        Gbyte = 3
    }
}