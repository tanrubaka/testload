﻿using System;

namespace TestLoad.Utils
{
    public static class TimeConvert
    {
        public static DateTime Convert(DateTime date, int timeZone)
        {
            return date.AddHours(timeZone);
        }
    }
}