﻿using System.IO;
using System.Threading.Tasks;
using TestLoad.Configuration;

namespace TestLoad.Utils.Pictures
{
    public static class FileLoader
    {
        public static async Task<string> Upload(string fileName,byte[] fileArray)
        {
            var fileExt = Path.GetExtension(fileName)??string.Empty;
            var savedFileName = Common.GeneratePictureName(fileExt.ToLower());
            var filePath = Common.GetFilePath(savedFileName, AppConfig.FilePholder);

            using (System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
            {
                await fs.WriteAsync(fileArray, 0, fileArray.Length);
                return savedFileName;
            }
        }

        public static string GetFilePath(string fileName)
        {
            return Common.GetFilePath(fileName, AppConfig.FilePholder);
        }

        public static string GetPictureUrl(string file)
        {
            if (!string.IsNullOrEmpty(file) && IsPictureExist(file))
            {
                return GetPictureUrl_Internal(file);
            }
            return GetDefaultPictureUrl();
        }

        public static string GetDefaultPictureUrl()
        {
            return Common.GetFileUrl("Default.png", AppConfig.FilePholder);
        }

        private static bool IsPictureExist(string file)
        {
            return Common.IsFileExist(file, AppConfig.FilePholder);
        }

        private static string GetPictureUrl_Internal(string file)
        {
            return Common.GetFileUrl(file, AppConfig.FilePholder);
        }

    }
}
