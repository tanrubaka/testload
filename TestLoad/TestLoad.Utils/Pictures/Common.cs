﻿using System;
using System.IO;
using System.Web;

namespace TestLoad.Utils.Pictures
{
    internal static class Common
    {
        private const string PathPrefix = "~/";
        private const string URLSeparator = "/";
        private const string PathSeparator = "\\";

        

        internal static bool IsFileExist(string fileName, string folder)
        {
            var filepath = GetFilePath(fileName, folder);
            FileInfo file = new FileInfo(filepath);
            if (file.Exists)
                return true;
            return false;
        }

        internal static string GetFileUrl(string file, string url)
        {
            return URLSeparator + url + Common.URLSeparator + file;
        }

        internal static string GetFilePath(string file, string folder)
        {
            return HttpContext.Current.Server.MapPath(PathSeparator + folder + Common.PathSeparator + file);
        }

        public static string GeneratePictureName(string extension)
        {
            return DateTime.UtcNow.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid() + extension;
        }
    }
}