﻿using System;
using System.Text;

namespace TestLoad.Utils
{
    public static class SizeConvert
    {
        private const long Divider = 1024;

        public static string GetString(long size)
        {
            var index = 0;
            while (size>=Divider && index<(int)Enums.ByteSize.Gbyte)
            {
                size = size/Divider;
                index++;
            }
            var res = new StringBuilder();
            res.Append(size);

            switch ((Enums.ByteSize)index)
            {
                case Enums.ByteSize.Byte:
                    res.AppendFormat(" {0}", Resources.Resource.Byte);
                    break;
                case Enums.ByteSize.Kbyte:
                    res.AppendFormat(" {0}", Resources.Resource.Kbyte);
                    break;
                case Enums.ByteSize.Mbyte:
                    res.AppendFormat(" {0}", Resources.Resource.Mbyte);
                    break;
                case Enums.ByteSize.Gbyte:
                    res.AppendFormat(" {0}", Resources.Resource.Gbyte);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return res.ToString();
        }
    }
}